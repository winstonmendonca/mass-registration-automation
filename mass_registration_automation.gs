/* Set Triggers for 
 * SplitData - On Form Submit
 * sendNotificiations - Daily 8-9 AM
 * archiveOldSheets - Daily 10-11 PM
 * printListToday - Trigger set to couple of hours before the Mass/Event (Daily 11-12 PM)
 * compileMassHistory - Daily 9-10 PM
 * 
 * Operation:
 * SplitData
 * 1. Called for every new form submission
 * 2. Adds new records to each sheet (or updates repeated records)
 * 3. If sheet has no status, records are marked as "New" for new registrants or "On hold" for returning registrants. This is done to give preference to those who have not attended a Mass in the past.
 * 4. If sheet is set to Auto Accept, Mailing status for each new registration is decided based on the available capacity and their last accepted Mass as compared to other returning registrants.
 * 5. If sheet is set to Send Responses, every new registration will be directly accepted till the availability capacity is reached.
 * 
 * SendNotifications
 * 1. Called every morning between 8:00 and 9:00 AM
 * 2. Runs through all sheets individually. Sets data validation for the Mailing Status cell (H2) if it wasn't previously set.
 * 3. If the current sheet is less than four days away, Mailing Status for that sheet is set to Send Responses.
 * 4. If current sheet is set to Send Responses, send notifications for all entries in current sheet based on their decision cell.
 * 5. If sheet capacity has been reached, remove the entry from the form if it still exists there. Do this without notifying anyone.
 */

const TIMESTAMP_COL = 1;
const NAME_COL = 2;
const NUM_ATTEND_COL = 3;
const EMAIL_COL = 4;
const CONTACT_NO_COL = 5;
const PREFER_COL = 6;
const HISTORY_COL = 7;
const DECISION_COL = 8;
const LAST_ACCEPT_COL = 9;
const DECISION_RANGE = "H:H";
const MAIL_STATUS_CELL = "H2";
const WAITLIST_COUNT_CELL = "C2";
const ACCEPTED_COUNT_CELL = "E2";
const MS_IN_DAY = (1000 * 60 * 60 * 24);
const capacity = 50;
const regulators = "winsmen.wm@gmail.com, widianta.gomulya@gmail.com";
const greeters = "winsmen.wm@gmail.com,agadaonoja@gmail.com,lsha_357@hotmail.com,apmechel@yahoo.com";

const SORT_ORDER = [{column: LAST_ACCEPT_COL, ascending: true}, 
                  {column: TIMESTAMP_COL, ascending: true}]

function test() {

  createNewSheet("Wednesday 29th December 2021");

  /*var text = "<html><body>Dear " + "Winston" + ",<br><br>Unfortunately, something went wrong with your registration submission. " + 
        "Could you please re-submit your registration request using the same " + 
          "<a href=\"https://docs.google.com/forms/d/e/1FAIpQLSdDnclrsbQT20jInHM4aneIA9rnZhE8lmRkYeT_cOSJdBm4OA/viewform?usp=sf_link\" rel=\"noopener noreferrer\" target=\"_blank\">registration form</a>?" + 
            "<br><br>Apologies for the inconvenience, and thank you for your understanding!<br><br>Sincerely,<br><br>English Mass Eindhoven Team</body></html>";
  MailApp.sendEmail({to:"winsmen.wm@gmail.com", subject:"English Mass Eindhoven Registration: Error in form submission", htmlBody: text});
  var sheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName("Saturday 28th November");
  var cell = sheet.getRange('I15').getValue();
  Logger.log(cell);*/

/*  var today = new Date();
  today.setHours(0,0,0,0);
  Logger.log("Today is: " + today.toString());
  var i;
  var sheets = SpreadsheetApp.getActiveSpreadsheet().getSheets();
  for (i = 0; i < sheets.length; i++) {
    var sheetName = sheets[i].getName();
    if (sheetName == "Form Responses 1" || sheetName == "History" || sheetName == "Status" || sheetName == "Test") { 
      continue;
    }
    var sheetDate = parseDate(sheets[i].getName());
    Logger.log("Looking at " + sheets[i].getName() + ":" + sheetDate.toString());
    Logger.log((sheetDate - today)/MS_IN_DAY);
    if ((sheetDate - today)/MS_IN_DAY < 4) {
      Logger.log("Time to send notifications");
      /* var archive = SpreadsheetApp.openById("1MgVQUC6BbTO5xtk5Y1dHqg1uPLsQVPCL1bRFYLfzekY");
      sheets[i].copyTo(archive);
      SpreadsheetApp.getActiveSpreadsheet().deleteSheet(sheets[i]); * /
    }
  }*/
}

// Reformat the options string to match sheet names (remove commas, timings, and additional text)
function reformat(str) {
  var i = str.indexOf(',',0);
  while (i != -1) {
    str = str.substring(0,i) + str.substring(i+1);
    var j = str.indexOf('(',i+1)-1;
    i = str.indexOf(',',j);
    if (i == -1) {
      str = str.substring(0,j);
      break;
    }
    str = str.substring(0,j) + ';' + str.substring(i+2);
    i = str.indexOf(',',j);
  }
  return str
}

// Reformat the options string to separate every event with a semicolon
function reformatSimple(str) {
  var i = str.indexOf(',',0);
  while (i != -1) {
    i = str.indexOf(',',i+1);
    if (i == -1) {
      break;
    }
    str = str.substring(0,i) + ';' + str.substring(i+2);
    i = str.indexOf(',',i+1);
  }
  return str
}

// Return month number from month name
function parseMonth(str) {
  switch (str) {
    case "January":
      return 1;
    case "February":
      return 2;
    case "March":
      return 3;  
    case "April":
      return 4;
    case "May":
      return 5;
    case "June":
      return 6;
    case "July":
      return 7;
    case "August":
      return 8;
    case "September":
      return 9;
    case "October":
      return 10;
    case "November":
      return 11;
    case "December":
      return 12; 
    default:
      return -1;
  }
}

// Parse date from Sheet name (<Day> <dd> <mmmmm> <yyyy>)
function parseDate(dateStr) {
  var parts = dateStr.split(" ");
  var dayIndex;
  var i;
  for (i = 0; i < parts.length; i++) {
    if (parts[i].charAt(0) >= '0' && parts[i].charAt(0) <= '9') {
      dayIndex = i;
      break;
    }
  }
  var date = new Date(parseInt(parts[dayIndex+2]),parseMonth(parts[dayIndex+1])-1,parseInt(parts[dayIndex]))
  return date;
  //  Logger.log(date);
  //  Logger.log(date>Date());
  //  var today = new Date();
  //  Logger.log(date>today);
}

// Check if a given sheet name exists
function sheetExists(name) {
  var sheets = SpreadsheetApp.getActiveSpreadsheet().getSheets();
  var i;
  for (i = 0; i < sheets.length; i++) {
    if (sheets[i].getName() == name) {
      return true;
    }
  }
  return false;
}

function customNotification() {
  Logger.log("Running custom email dispatcher");
  var sheets = SpreadsheetApp.getActiveSpreadsheet().getSheets();
  var i;
  for (i = 0; i < sheets.length; i++) {
    if (sheets[i].getName() !== "Form Responses 1" && sheets[i].getName() !=="Status") {
      //Check for mailing status cell (H2)
      var rule = sheets[i].getRange(MAIL_STATUS_CELL).getDataValidation();
      if (rule == null) {
        rule = SpreadsheetApp.newDataValidation().requireValueInRange(SpreadsheetApp.getActiveSpreadsheet().getSheetByName("Status").getRange('B:B')).build();
        sheets[i].getRange(MAIL_STATUS_CELL).setDataValidation(rule);
      }
      
      if (sheets[i].getRange(MAIL_STATUS_CELL).getValue() == "Send responses" || sheets[i].getRange(MAIL_STATUS_CELL).getValue() == "Auto accept") {
        Logger.log("Sending emails for " + sheets[i].getName());
        var lastRow = sheets[i].getLastRow();
        var data = sheets[i].getRange(3, 1, lastRow-2, 8).getValues();
        var j;
        var sheetName = sheets[i].getName();
        for (j = 0; j < data.length; j++) {
          if (data[j][7] == "Accepted") {
            var name = data[j][NAME_COL-1];
            var numAttending = data[j][NUM_ATTEND_COL-1];
            var curEmail = data[j][EMAIL_COL-1];
            var options = reformat(data[j][PREFER_COL-1]).split(";");
            var k;
            var eventName = "Null";
            for (k = 0; k < options.length; k++) {
              if (options[k] == sheetName) {
                eventName = reformatSimple(data[j][PREFER_COL-1]).split(";")[k];
                break;
              }
            }
            var text = "Greetings, " + name + "!<br><br>" +
              "Thank you for your registration. <strong>We would like to confirm your registration for " + numAttending.toString() +
                " person(s) for " + eventName + " at SINT JORISKERK.</strong><br><br>" +
                "To accommodate for the increased number of worshippers on Easter Sunday, there will be two English Masses celebrated at 12:30 pm on the 4th of April, and the registrations have been split accordingly. You are therefore requested to attend the Mass at <strong>SINT JORISKERK</strong> (Sint Jorislaan 51, 5614 AA).<br><br>" + 
                  "Here are some guidelines for you to keep in mind before coming to church:<br>" +
                    "<ul><li>If you are sick or have any symptoms of the coronavirus, please stay at home.</li>" +
                      "<li>Please wear a mask while coming to church, You may take your mask off once you are seated.</li>" +
                        "<li>Sanitize your hands at the entrance of the church.</li>" +
                          "<li>Maintain a distance of 1.5m from others at all times.</li>" +
                            "<li>Refrain from singing any hymns out aloud during mass. You may recite the responses to prayers.</li></ul><br>" +
                              "<strong>If you are unable to make it, please reply to this email so that we can offer your slot to others." + 
                                "</strong> We look forward to seeing you at church!<br><br><br>Sincerely,<br><br>English Mass Eindhoven Team";
            MailApp.sendEmail({to: curEmail, subject: "Easter Mass in English at SINT JORISKERK: " + eventName, htmlBody:  '<html><body>' + text + '</body></html>'});
            sheets[i].getRange(j+3,DECISION_COL).setValue("Acceptance email sent");
          }
          if (data[j][DECISION_COL-1] == "Rejected") {
            var name = data[j][NAME_COL-1];
            var numAttending = data[j][NUM_ATTEND_COL-1];
            var curEmail = data[j][EMAIL_COL-1];
            var options = reformat(data[j][PREFER_COL-1]).split(";");
            var k;
            var eventName = "Null";
            for (k = 0; k < options.length; k++) {
              if (options[k] == sheetName) {
                eventName = reformatSimple(data[j][PREFER_COL-1]).split(";")[k];
                break;
              }
            }
            var text = "Greetings, " + name + "!<br><br>Thank you for your registration. <strong>Unfortunately, we are not able to accommodate you for " +
              eventName + " as we have reached our limit of " + capacity.toString() + " worshippers.</strong> However, you will be given priority for the next upcoming mass that you are registered for. " + 
                "If you haven't already done so, you can sign up for the upcoming masses on the same "+
                  "<a href=\"https://docs.google.com/forms/d/e/1FAIpQLSdDnclrsbQT20jInHM4aneIA9rnZhE8lmRkYeT_cOSJdBm4OA/viewform?usp=sf_link\" rel=\"noopener noreferrer\" target=\"_blank\">registration form</a>. " + 
                    "You can also follow the English Mass live on our <a href=\"https://www.youtube.com/channel/UCTWIrm6Mnbh2y81ZUVbUJFg\" rel=\"noopener noreferrer\" target=\"_blank\">YouTube channel</a>.<br><br>" +
                      "Thank you for your continued prayers and support, and hope to see you soon!<br><br>Sincerely,<br><br>English Mass Eindhoven Team";
            MailApp.sendEmail({to: curEmail, subject: "English Mass Registration Response: " + eventName, htmlBody:  '<html><body>' + text + '</body></html>'});
            sheets[i].getRange(j+3,DECISION_COL).setValue("Rejection email sent");
          }
        }
        sheets[i].getRange(MAIL_STATUS_CELL).setValue("Send responses");
      }
    }
  }
  Logger.log("Custom email dispatching complete");
}

// Automated confirmation/rejection email shot
function sendNotifications() {
  Logger.log("Running email dispatcher");
  var sheets = SpreadsheetApp.getActiveSpreadsheet().getSheets();
  var i;
  for (i = 0; i < sheets.length; i++) {
    if (sheets[i].getName() !== "Form Responses 1" && sheets[i].getName() !=="Status") {
      //Check for mailing status cell (H2) validation rule
      var rule = sheets[i].getRange(MAIL_STATUS_CELL).getDataValidation();
      if (rule == null) {
        rule = SpreadsheetApp.newDataValidation().requireValueInRange(SpreadsheetApp.getActiveSpreadsheet().getSheetByName("Status").getRange('B:B')).build();
        sheets[i].getRange(MAIL_STATUS_CELL).setDataValidation(rule);
      }

      // Set mailing status cell based on how many days left till the current spreadsheet's event
      var sheetName = sheets[i].getName();
      var sheetDate = parseDate(sheetName);
      var today = new Date();
      today.setHours(0,0,0,0);
      if ((sheetDate - today)/MS_IN_DAY < 4) {
        if (sheets[i].getRange(MAIL_STATUS_CELL).getValue() != "Send responses") {
          sheets[i].getRange([MAIL_STATUS_CELL]).setValue("Send responses");
        }
      }
      
      // If mailing status cell is set to send responses, send pending responses for current sheet
      if (sheets[i].getRange(MAIL_STATUS_CELL).getValue() == "Send responses") {
        Logger.log("Sending emails for " + sheets[i].getName());
        var lastRow = sheets[i].getLastRow();
        var data = sheets[i].getRange(3, 1, lastRow-2, 8).getValues();
        var j;
        for (j = 0; j < data.length; j++) {
          if (data[j][DECISION_COL-1] == "On hold") {
            if (sheets[i].getRange(ACCEPTED_COUNT_CELL).getValue() < capacity) {
              sheets[i].getRange(j+3,DECISION_COL).setValue("Accepted");
              data[j][DECISION_COL-1] = "Accepted";
            }
            else {
              sheets[i].getRange(j+3,DECISION_COL).setValue("Rejected");
              data[j][DECISION_COL-1] = "Rejected";
            }
          }
          if (data[j][DECISION_COL-1] == "New") {
            sheets[i].getRange(j+3,DECISION_COL).setValue("Accepted");
            data[j][DECISION_COL-1] = "Accepted";
          }
          if (data[j][DECISION_COL-1] == "Accepted") {
            var name = data[j][NAME_COL-1];
            var numAttending = data[j][NUM_ATTEND_COL-1];
            var curEmail = data[j][EMAIL_COL-1];
            var options = reformat(data[j][PREFER_COL-1]).split(";");
            var k;
            var eventName = "Null";
            for (k = 0; k < options.length; k++) {
              if (options[k] == sheetName) {
                eventName = reformatSimple(data[j][PREFER_COL-1]).split(";")[k];
                break;
              }
            }
            var text = "Greetings, " + name + "!<br><br>" +
              "Thank you for your registration. <strong>We would like to confirm your registration for " + numAttending.toString() +
                " person(s) for " + eventName + " at Sint Lambertuskerk.</strong><br><br>" +
                  "Here are some guidelines for you to keep in mind before coming to church:<br>" +
                    "<ul><li>If you are sick or have any symptoms of the coronavirus, please stay at home.</li>" +
                      "<li>Please wear a mask while coming to church, You may take your mask off once you are seated.</li>" +
                        "<li>Sanitize your hands at the entrance of the church.</li>" +
                          "<li>Maintain a distance of 1.5m from others at all times.</li>" +
                            "<li>Refrain from singing any hymns out aloud during mass. You may recite the responses to prayers.</li></ul><br>" +
                              "<strong>If you are unable to make it, please reply to this email so that we can offer your slot to others." + 
                                "</strong> We look forward to seeing you at church!<br><br><br>Sincerely,<br><br>English Mass Eindhoven Team";
            MailApp.sendEmail({to: curEmail, subject: "English Mass Registration Confirmation: " + eventName, htmlBody:  '<html><body>' + text + '</body></html>'});
            sheets[i].getRange(j+3,DECISION_COL).setValue("Acceptance email sent");
          }
          if (data[j][7] == "Rejected") {
            var name = data[j][NAME_COL-1];
            var numAttending = data[j][NUM_ATTEND_COL-1];
            var curEmail = data[j][EMAIL_COL-1];
            var options = reformat(data[j][PREFER_COL-1]).split(";");
            var k;
            var eventName = "Null";
            for (k = 0; k < options.length; k++) {
              if (options[k] == sheetName) {
                eventName = reformatSimple(data[j][PREFER_COL-1]).split(";")[k];
                break;
              }
            }
            var text = "Greetings, " + name + "!<br><br>Thank you for your registration. <strong>Unfortunately, we are not able to accommodate you for " +
              eventName + " as we have reached our limit of " + capacity.toString() + " worshippers.</strong> However, you will be given priority for the next upcoming mass that you are registered for. " + 
                "If you haven't already done so, you can sign up for the upcoming masses on the same "+
                  "<a href=\"https://docs.google.com/forms/d/e/1FAIpQLSdDnclrsbQT20jInHM4aneIA9rnZhE8lmRkYeT_cOSJdBm4OA/viewform?usp=sf_link\" rel=\"noopener noreferrer\" target=\"_blank\">registration form</a>. " + 
                    "You can also follow the English Mass live on our <a href=\"https://www.youtube.com/channel/UCTWIrm6Mnbh2y81ZUVbUJFg\" rel=\"noopener noreferrer\" target=\"_blank\">YouTube channel</a>.<br><br>" +
                      "Thank you for your continued prayers and support, and hope to see you soon!<br><br>Sincerely,<br><br>English Mass Eindhoven Team";
            MailApp.sendEmail({to: curEmail, subject: "English Mass Registration Response: " + eventName, htmlBody:  '<html><body>' + text + '</body></html>'});
            sheets[i].getRange(j+3,DECISION_COL).setValue("Rejection email sent");
          }
        }
        sheets[i].getRange(MAIL_STATUS_CELL).setValue("Send responses");
        if (sheets[i].getRange(ACCEPTED_COUNT_CELL).getValue() >= capacity) {
          Logger.log("Reached max occupancy in " + sheetName);
          //Edit form
          Logger.log("Removing item from choice list");
          var form = FormApp.openById("1ODPBRcFqCt5alEpy7BQBjDQmqF0AU_cHGxfsgF0ReFI");
          var choices = form.getItems()[4].asCheckboxItem().getChoices();
          var j;
          Logger.log("Original list length: " + choices.length.toString());
          for (j = 0; j < choices.length; j++) {
            // if option matches name of sheet
            if (reformat(choices[j].getValue()) === sheets[i].getName()) {
              choices.splice(j,1);
            }
          }
          Logger.log("Revised list length: " + choices.length.toString());
          form.getItems()[4].asCheckboxItem().setChoices(choices);
        }
      }
    }
  }
  Logger.log("Email dispatching complete");
}

// Automated print of finalised lists
function printList() {
  Logger.log("Running list printer");
  var sheets = SpreadsheetApp.getActiveSpreadsheet().getSheets();
  var i;
  for (i = 0; i < sheets.length; i++) {
    if (sheets[i].getRange(MAIL_STATUS_CELL).getValue() == "Print list") {
      var printSheet = SpreadsheetApp.openById("1bnGjox6LbbbLj4vGMpwfTjeeNOhEPgPB4Ppl4PQPce8").getSheets()[0];
      var lastRow = printSheet.getLastRow();
      var lastCol = printSheet.getLastColumn();
      sheets[i].sort(NAME_COL);
      printSheet.getRange("A1").setValue(sheets[i].getName());  // Set Title of print sheet to the current sheet name
      printSheet.getRange(4,1,lastRow,lastCol).clear();   // Clear all old data in the print sheet
      lastRow = sheets[i].getLastRow();
      var names = sheets[i].getRange(3, NAME_COL, sheets[i].getLastRow()-2, 1).getValues();
      var numAttend = sheets[i].getRange(3, NUM_ATTEND_COL, sheets[i].getLastRow()-2, 1).getValues();
      var decisions = sheets[i].getRange(3, DECISION_COL, sheets[i].getLastRow()-2, 1).getValues();
      var j;
      var text = "<table style=\"width: 100%; border-collapse: collapse; border: 1px solid;\"><tbody><tr><td colspan=\"2\" style=\"width: 100%; border: 1px solid;\"><div style=\"text-align: center;\">" + sheets[i].getName() + "</div></td></tr>";
      for (j = 0; j < names.length; j++) {
        if (decisions[j] == "Acceptance email sent") {
          printSheet.appendRow([names[j][0],numAttend[j][0]]);
        }
      }
      text += "<tr><td style=\"width: 50%; border: 1px solid;\"><div style=\"text-align: center;\">Total</div></td><td style=\"width: 50%; border: 1px solid;\"><div style=\"text-align: center;\">" + printSheet.getRange(3, 2).getValue().toString() + "</div></td></tr>";
      text += "<tr><td style=\"width: 50%; border: 1px solid;\"><div style=\"text-align: center;\">Name</div></td><td style=\"width: 50%; border: 1px solid;\"><div style=\"text-align: center;\">Number of Attendees</div></td></tr>";
      for (j = 0; j < names.length; j++) {
        if (decisions[j] == "Acceptance email sent") {
          text += "<tr><td style=\"width: 50.0000%; border: 1px solid;\">" + names[j][0] + "</td><td style=\"width: 50.0000%; border: 1px solid;\"><div style=\"text-align: center;\">" + numAttend[j][0] + "</div></td></tr>";
        }
      }
      text += "</tbody></table>";
      var theBlob = SpreadsheetApp.openById("1bnGjox6LbbbLj4vGMpwfTjeeNOhEPgPB4Ppl4PQPce8").getBlob().getAs('application/pdf').setName(sheets[i].getName() + " - Final List");
      //var newFile = folder.createFile(theBlob);
      sheets[i].getRange(MAIL_STATUS_CELL).setValue("List printed")
      MailApp.sendEmail({to:greeters, subject:theBlob.getName(), attachments:theBlob, htmlBody:text});
    }
  }
}

// Print today's list
function printListToday() {
  Logger.log("Running list printer for today");
  var sheets = SpreadsheetApp.getActiveSpreadsheet().getSheets();
  var today = new Date();
  var i;
  for (i = 0; i < sheets.length; i++) {
    var sheetName = sheets[i].getName();
    if (sheetName == "Form Responses 1" || sheetName == "History" || sheetName == "Status" || sheetName == "Test") {
      continue;
    }
    var sheetDate = parseDate(sheetName);
    if (sheetDate.toDateString() == today.toDateString()) {
      Logger.log("Printing " + sheetName);
      var printSheet = SpreadsheetApp.openById("1bnGjox6LbbbLj4vGMpwfTjeeNOhEPgPB4Ppl4PQPce8").getSheets()[0];
      var lastRow = printSheet.getLastRow();
      var lastCol = printSheet.getLastColumn();
      sheets[i].sort(NAME_COL);
      printSheet.getRange("A1").setValue(sheets[i].getName());
      printSheet.getRange(4,1,lastRow,lastCol).clear();
      lastRow = sheets[i].getLastRow();
      var names = sheets[i].getRange(3, NAME_COL, sheets[i].getLastRow()-2, 1).getValues();
      var numAttend = sheets[i].getRange(3, NUM_ATTEND_COL, sheets[i].getLastRow()-2, 1).getValues();
      var decisions = sheets[i].getRange(3, DECISION_COL, sheets[i].getLastRow()-2, 1).getValues();
      var j;
      var text = "<table style=\"width: 100%; border-collapse: collapse; border: 1px solid;\"><tbody><tr><td colspan=\"2\" style=\"width: 100%; border: 1px solid;\"><div style=\"text-align: center;\">" + sheets[i].getName() + "</div></td></tr>";
      for (j = 0; j < names.length; j++) {
        if (decisions[j] == "Acceptance email sent") {
          printSheet.appendRow([names[j][0],numAttend[j][0]]);
        }
      }
      text += "<tr><td style=\"width: 50%; border: 1px solid;\"><div style=\"text-align: center;\">Total</div></td><td style=\"width: 50%; border: 1px solid;\"><div style=\"text-align: center;\">" + printSheet.getRange(3, 2).getValue().toString() + "</div></td></tr>";
      text += "<tr><td style=\"width: 50%; border: 1px solid;\"><div style=\"text-align: center;\">Name</div></td><td style=\"width: 50%; border: 1px solid;\"><div style=\"text-align: center;\">Number of Attendees</div></td></tr>";
      for (j = 0; j < names.length; j++) {
        if (decisions[j] == "Acceptance email sent") {
          text += "<tr><td style=\"width: 50.0000%; border: 1px solid;\">" + names[j][0] + "</td><td style=\"width: 50.0000%; border: 1px solid;\"><div style=\"text-align: center;\">" + numAttend[j][0] + "</div></td></tr>";
        }
      }
      text += "</tbody></table>";
      var theBlob = SpreadsheetApp.openById("1bnGjox6LbbbLj4vGMpwfTjeeNOhEPgPB4Ppl4PQPce8").getBlob().getAs('application/pdf').setName(sheets[i].getName() + " - Final List");
      //var newFile = folder.createFile(theBlob);
      sheets[i].getRange(MAIL_STATUS_CELL).setValue("List printed")
      MailApp.sendEmail({to:greeters, subject:theBlob.getName(), attachments:theBlob, htmlBody:text});
      
      // Remove entry from form if it still exists
      Logger.log("Removing item from choice list");
      var form = FormApp.openById("1ODPBRcFqCt5alEpy7BQBjDQmqF0AU_cHGxfsgF0ReFI");
      var choices = form.getItems()[4].asCheckboxItem().getChoices();
      var j;
      Logger.log("Original list length: " + choices.length.toString());
      for (j = 0; j < choices.length; j++) {
        // if option matches name of sheet
        if (reformat(choices[j].getValue()) === sheets[i].getName()) {
          choices.splice(j,1);
        }
      }
      Logger.log("Revised list length: " + choices.length.toString());
      form.getItems()[4].asCheckboxItem().setChoices(choices)
    }
  }
  // Compile Mass History from finalised mass(es) and archive sheet(s)
  compileMassHistory();
  archiveOldSheets();
}

// Compile last "Accepted" mass list
function compileMassHistory() {
  var sheets = SpreadsheetApp.getActiveSpreadsheet().getSheets();
  // Create Status sheet if it does not exist
  var historySheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName("History");
  if (historySheet == null) {
    Logger.log("History sheet missing. Creating new sheet and populating with all available entries.");
    historySheet = SpreadsheetApp.getActiveSpreadsheet().insertSheet("History",1);
  }
  //Populate from available sheets
  var i;
  var today = new Date();
  for (i = 0; i < sheets.length; i++) {
    if (sheets[i].getName() == "Form Responses 1" || sheets[i].getName() == "Status" || sheets[i].getName() == "History"|| sheets[i].getName() == "Test") {
      continue;
    }
    //get date in Date format variable
    var sheetDate = parseDate(sheets[i].getName());
    if (sheetDate > today) {
      continue;
    }
    Logger.log(sheets[i].getName() + "_" + sheetDate.toDateString());
    //get all entries in current sheet
    var sheetRows = sheets[i].getLastRow();
    var names = sheets[i].getRange(3, NAME_COL, sheetRows-2, 1).getValues();
    var emails = sheets[i].getRange(3, EMAIL_COL, sheetRows-2, 1).getValues();
    var decisions = sheets[i].getRange(3, DECISION_COL, sheetRows-2, 1).getValues();
    var histEmails = historySheet.getRange(1, 2, historySheet.getLastRow(), 1).getValues();
    var j;
    for (j = 0; j < emails.length; j++) {
      var pos = -1;
      var k;
      for (k = 0; k < histEmails.length; k++) {
        if (histEmails[k][0] == emails[j][0]) {
          pos = k+1;
          break;
        }
      }
      if (pos == -1) {
        var rowNum = historySheet.getLastRow()+1;
        historySheet.getRange(rowNum, 1).setValue(names[j][0]);
        historySheet.getRange(rowNum, 2).setValue(emails[j][0]);
        if (decisions[j][0] == "Acceptance email sent" || decisions[j][0] == "Accepted" || decisions[j][0] == "Volunteer") {
          historySheet.getRange(rowNum, 3).setValue(sheetDate.toDateString());
        }
      } else { //Look for old entry and update date information
        var oldDate = new Date(historySheet.getRange(pos, 3).getValue());
        if (sheetDate > oldDate && (decisions[j][0] == "Acceptance email sent" || decisions[j][0] == "Accepted" || decisions[j][0] == "Volunteer")) {
          historySheet.getRange(pos, 3).setValue(sheetDate.toDateString());
        }
      }
    }
    Logger.log("Sorting " + sheets[i].getName());
    sheetRows = sheets[i].getLastRow();    
    sheets[i].getRange(3,1,sheetRows-2,LAST_ACCEPT_COL).sort(SORT_ORDER);
  }
}

function archiveOldSheets() {
  var today = new Date();
  today.setHours(0,0,0,0);
  var i;
  var sheets = SpreadsheetApp.getActiveSpreadsheet().getSheets();
  for (i = 0; i < sheets.length; i++) {
    var sheetName = sheets[i].getName();
    if (sheetName == "Form Responses 1" || sheetName == "History" || sheetName == "Status" || sheetName == "Test") { 
      continue;
    }
    var sheetDate = parseDate(sheets[i].getName());
    if (today > sheetDate) {
      Logger.log("Moving " + sheets[i].getName() + " to Archive");
      var archive = SpreadsheetApp.openById("1MgVQUC6BbTO5xtk5Y1dHqg1uPLsQVPCL1bRFYLfzekY");
      sheets[i].copyTo(archive);
      SpreadsheetApp.getActiveSpreadsheet().deleteSheet(sheets[i]);
    }
  }
}

function trimAllSortedData() {
  var sheets = SpreadsheetApp.getActiveSpreadsheet().getSheets();
  var i;
  for (i = 0; i < sheets.length; i++) {
    if (sheets[i].getName() == "Form Responses 1" || sheets[i].getName() == "Status" || sheets[i].getName() == "History") {
      continue;
    }
    var j;
    for (j = 3; j < sheets[i].getLastRow(); j++) {
      if (sheets[i].getRange(j, NAME_COL).getValue().charAt(0) == " " || sheets[i].getRange(j, NAME_COL).getValue().substr(-1) == " ") {
        Logger.log("Found whitespace for name at " + i.toString() + "," + j.toString());
      }
      if (sheets[i].getRange(j, EMAIL_COL).getValue().charAt(0) == " " || sheets[i].getRange(j, EMAIL_COL).getValue().substr(-1) == " ") {
        Logger.log("Found whitespace for email at " + i.toString() + "," + j.toString());
      }
      sheets[i].getRange(j, NAME_COL).setValue(sheets[i].getRange(j,NAME_COL).getValue().trim());
      sheets[i].getRange(j, EMAIL_COL).setValue(sheets[i].getRange(j,EMAIL_COL).getValue().trim());
    }
  }
}

// Add Conditional Formatting to Status column in a sheet
function addConditionalFormatting(sheet,range) {
  var rules = sheet.getConditionalFormatRules();
  var rule = SpreadsheetApp.newConditionalFormatRule()
    .whenCellEmpty()
    .setBackground("#FFFFFF")
    .setRanges([range])
    .build();
  rules.push(rule);
  rule = SpreadsheetApp.newConditionalFormatRule()
    .whenTextEqualTo("On hold")
    .setBackground("#F9CB9C")
    .setRanges([range])
    .build();
  rules.push(rule);
  rule = SpreadsheetApp.newConditionalFormatRule()
    .whenTextEqualTo("New")
    .setBackground("#9900FF")
    .setFontColor("#FFFFFF")
    .setRanges([range])
    .build();
  rules.push(rule);
  rule = SpreadsheetApp.newConditionalFormatRule()
    .whenTextEqualTo("Accepted")
    .setBackground("#b6d7a8")
    .setRanges([range])
    .build();
  rules.push(rule);
  rule = SpreadsheetApp.newConditionalFormatRule()
    .whenTextEqualTo("Rejected")
    .setBackground("#ea9999")
    .setRanges([range])
    .build();
  rules.push(rule);
  rule = SpreadsheetApp.newConditionalFormatRule()
    .whenTextEqualTo("Acceptance email sent")
    .setBackground("#00FF00")
    .setRanges([range])
    .build();
  rules.push(rule);
  rule = SpreadsheetApp.newConditionalFormatRule()
    .whenTextEqualTo("Rejection email sent")
    .setBackground("#FF0000")
    .setRanges([range])
    .build();
  rules.push(rule);
  rule = SpreadsheetApp.newConditionalFormatRule()
    .whenTextEqualTo("Volunteer")
    .setBackground("#4a86e8")
    .setRanges([range])
    .build();
  rules.push(rule);
  sheet.setConditionalFormatRules(rules);
}

// Create a new sheet with the given name
function createNewSheet(name) {
  var sheetIndex = SpreadsheetApp.getActiveSpreadsheet().getSheets().length + 1;
  var newSheet = SpreadsheetApp.getActiveSpreadsheet().insertSheet(name,sheetIndex);
  
  // Set values of first two rows + freeze first two rows
  var responsesForm = SpreadsheetApp.getActiveSpreadsheet().getSheetByName("Form Responses 1");
  var numCols = responsesForm.getLastColumn();
  var firstRows = responsesForm.getRange(1,1,1,numCols);
  newSheet.appendRow(firstRows.getValues()[0]);
  newSheet.setFrozenRows(2);

  // Set formula for Total cell
  newSheet.getRange(WAITLIST_COUNT_CELL).setFormula("=SUM(C3:C)");
  newSheet.getRange("B2").setValue("Total");

  // Set formula for Total Accepted/New cell
  newSheet.getRange(ACCEPTED_COUNT_CELL).setFormula("=SUMIF(H3:H,\"Accepted\",C3:C)+SUMIF(H3:H,\"Acceptance email sent\",C3:C)+SUMIF(H3:H,\"New\",C3:C)");
  newSheet.getRange("D2").setValue("Total Accepted/New");
  
  // Set Data validation rule and conditional formatting for Status cell
  var range = SpreadsheetApp.getActiveSpreadsheet().getSheetByName("Status").getRange('A:A');
  var rule = SpreadsheetApp.newDataValidation().requireValueInRange(range).build();
  newSheet.getRange("H3:H").setDataValidation(rule);
  addConditionalFormatting(newSheet,newSheet.getRange(DECISION_RANGE));
  range = SpreadsheetApp.getActiveSpreadsheet().getSheetByName("Status").getRange('B:B');
  rule = SpreadsheetApp.newDataValidation().requireValueInRange(range).build();
  newSheet.getRange(MAIL_STATUS_CELL).setDataValidation(rule);

  // // Set sheet status to Auto Accept
  // newSheet.getRange(MAIL_STATUS_CELL).setValue("Auto accept")
}

// Copy data to individual day spreadsheets
function splitData() {
  var main = SpreadsheetApp.getActiveSpreadsheet().getSheetByName("Form Responses 1");  // Main Sheet with original form responses
  var sheet = SpreadsheetApp.getActiveSpreadsheet().getSheets();  // Array of all sheets in the Spreadsheet
  var numRows = main.getLastRow();  // Last row of main responses
  var numCols = main.getLastColumn();  // Last column of main responses (will always be the same)
  var row = main.getRange(numRows, 1, 1, numCols).getValues();  // Get last row in main responses; this will be the row that was just entered when this function was triggered
  
  // Check if preference column is empty due to unknown bug in Google Forms
  if (row[0][PREFER_COL-1] == "") {
    // Check if email column is empty due to any bug, to avoid sending an email to an empty address
    if (row[0][EMAIL_COL-1] !== "") {
      // Generate HTML text for body of the email asking registrant to re-register due to error
      var text = "<html><body>Dear " + row[0][NAME_COL-1] + ",<br><br>Unfortunately, something went wrong with your registration submission. " + 
        "Could you please re-submit your registration request using the same " + 
          "<a href=\"https://docs.google.com/forms/d/e/1FAIpQLSdDnclrsbQT20jInHM4aneIA9rnZhE8lmRkYeT_cOSJdBm4OA/viewform?usp=sf_link\" rel=\"noopener noreferrer\" target=\"_blank\">registration form</a>?" + 
            " Apologies for the inconvenience, and thank you for your understanding!<br><br>Sincerely,<br><br>English Mass Eindhoven Team</body></html>";
      // Send HTML email using the MailApp class
      MailApp.sendEmail({to:row[0][EMAIL_COL-1], subject:"English Mass Eindhoven Registration: Error in form submission", htmlBody: text});
    }
    // Terminate the function as there is no valid data to operate on in this execution
    return;
  }
  
  // Parse dates into simple formatted strings matching the formatted sheet names
  var options = reformat(row[0][PREFER_COL-1]).split(";");  // 6th column (5 if first column is counted as 0) contains selected options
  var i;
  // Look for sheet with same date
  for (i = 0; i < options.length; i++) {
    if (sheetExists(options[i])) {
      Logger.log(options[i] + " exists");
    }
    else {
      Logger.log(options[i] + " does not exist. Creating new sheet.");  
      createNewSheet(options[i]);  // If sheet does not exist, create a new one using the event name as the sheet name
    }
    
    // Add entry to dated sheet
    var curSheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName(options[i]);
    var listEmails = curSheet.getRange(1, EMAIL_COL, curSheet.getLastRow(), 1).getValues();
    var j;
    var pos = -1;
    // Look for entry in current sheet
    for (j = 0; j < listEmails.length; j++) {
      if (listEmails[j][0] == row[0][EMAIL_COL-1]) {
        pos = j+1;
        break;
      }
    }
    // If record not found (new entry)
    if (pos == -1) {
      curSheet.appendRow(row[0]);
      var rowNum = curSheet.getLastRow();
      curSheet.getRange(rowNum,NAME_COL).setValue(String(curSheet.getRange(rowNum,NAME_COL).getValue()).trim());
      curSheet.getRange(rowNum,EMAIL_COL).setValue(curSheet.getRange(rowNum,EMAIL_COL).getValue().trim().toLowerCase());
      curSheet.getRange(rowNum,7).setFormula("=SUM($C$3:C"+rowNum.toString()+")");
      curSheet.getRange(rowNum,LAST_ACCEPT_COL).setFormula("=vlookup(D"+rowNum.toString()+",History!B:C,2,FALSE)");
      Logger.log("New Entry: Copied data to row " + rowNum.toString() + " in sheet " + options[i]);
      
      // Set Decision
      // Accept all
      // curSheet.getRange(rowNum,DECISION_COL).setValue("Accepted");
      // Accept based on Sheet status and capacity; see comments at the head of script for description of decisions made here
      if (curSheet.getRange(MAIL_STATUS_CELL).getValue() == "Send responses") {
        // Check capacity excluding the latest entry to accept or reject the latest entry
        if (parseInt(curSheet.getRange(ACCEPTED_COUNT_CELL).getValue())-parseInt(curSheet.getRange(rowNum,NUM_ATTEND_COL).getValue()) >= capacity) {
          curSheet.getRange(rowNum,DECISION_COL).setValue("Rejected");
        }
        else {
          curSheet.getRange(rowNum,DECISION_COL).setValue("Accepted");
        }
      }
      else if (curSheet.getRange(MAIL_STATUS_CELL).getValue() == "Auto Accept") {
        if (curSheet.getRange(rowNum,LAST_ACCEPT_COL).getValue() == "#N/A") {
          curSheet.getRange(rowNum,DECISION_COL).setValue("Accepted");
        }
        else {
          curSheet.getRange(rowNum,DECISION_COL).setValue("On Hold");
        }
      }
      else {
        if (curSheet.getRange(rowNum,LAST_ACCEPT_COL).getValue() == "#N/A") {
          curSheet.getRange(rowNum,DECISION_COL).setValue("New");
        }
        else {
          curSheet.getRange(rowNum,DECISION_COL).setValue("On hold");
        }
      }
      
      // Sort sheet based on last accepted mass and timestamp
      curSheet.getRange(3,1,rowNum-2,LAST_ACCEPT_COL).sort(SORT_ORDER);
    }
    else {
      var rowNum = curSheet.getLastRow();
      Logger.log("Old entry found at row " + pos + " - Updating number of attendees only")
      if (row[0][NUM_ATTEND_COL-1] != curSheet.getRange(pos, NUM_ATTEND_COL).getValue()) {
        curSheet.getRange(pos, NUM_ATTEND_COL).setValue(row[0][NUM_ATTEND_COL-1]);
      }
      curSheet.getRange(3,1,rowNum-2,LAST_ACCEPT_COL).sort(SORT_ORDER);
    }

    // Send notification already if sheet is set to auto accept
    if (curSheet.getRange(MAIL_STATUS_CELL).getValue() == "Send responses") {
      sendNotifications();
    }
    
    // If entries (Accepted + New) exceed capacity, notify via email and remove option from Form
    Logger.log("Number of Accepted/New responses in " + options[i] + ": " + curSheet.getRange(ACCEPTED_COUNT_CELL).getValue().toString());
    if (curSheet.getRange(ACCEPTED_COUNT_CELL).getValue() >= capacity) {
      Logger.log("Reached max occupancy in " + options[i]);
      //Edit form
      Logger.log("Removing item from choice list");
      var form = FormApp.openById("1ODPBRcFqCt5alEpy7BQBjDQmqF0AU_cHGxfsgF0ReFI");
      var choices = form.getItems()[4].asCheckboxItem().getChoices();
      var j;
      Logger.log("Original list length: " + choices.length.toString());
      for (j = 0; j < choices.length; j++) {
        // if option matches name of sheet
        if (reformat(choices[j].getValue()) === curSheet.getName()) {
          choices.splice(j,1);
        }
      }
      Logger.log("Revised list length: " + choices.length.toString());
      form.getItems()[4].asCheckboxItem().setChoices(choices);
      
      //Send email notification email to registration managers
      MailApp.sendEmail({to:regulators, subject:options[i]+" has reached maximum occupancy", htmlBody: "<html><body>" +
                        "Greetings!<br><br>" + options[i] + " is probably full and you may need to check on its status.<br><br>" +
                        "Total registrations: " + curSheet.getRange(WAITLIST_COUNT_CELL).getValue().toString() + 
                        "<br>Number of Accepted + New registrations: " + curSheet.getRange(ACCEPTED_COUNT_CELL).getValue().toString() + 
                        "<br><br>I have removed the event from the registration form for now, but you will have to add it back in case the capacity has not yet been reached." +
                         "<br><br>Sincerely,<br><br>Your Friendly Neighbourhood Automated Registration Handling System</body></html>"}); 
    }
  }
}